/**
 * Checks if two values are equal.
 * @param {*} testValue 
 * @param {*} trueValue 
 * @returns boolean.
 */
const assertEquals = (testValue, trueValue) => {
    return testValue === trueValue;
}

/**
 * Returns a function that returns the input value.
 * @param {*} word 
 * @returns a function
 */
const redundant = (word) => {
    return f = () => word;
};

const f1 = redundant("apple");
console.log(f1());
const f2 = redundant("pear");
const f3 = redundant("");

console.log(assertEquals(f1(), "apple"));
console.log(assertEquals(f2(), "pear"));
console.log(assertEquals(f3(), ""));