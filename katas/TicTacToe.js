/**
 * Transposes a 2D matrix.
 * @param {array} matrix - 2D array
 * @returns transposed array
 */
const transpose = (matrix) => {
    matrix = matrix[0].map((col,i) => matrix.map(row => row[i]));
    return matrix;
}

/**
 * Get diagonals of a 2D NxN matrix.
 * @param {Array} matrix - 2D NxN matrix
 * @returns Array with diagonals.
 */
const getDiagonals = (matrix) => {
    const numRows = matrix.length;
    const numCols = matrix[0].length;

    let rightDiagonal = [];
    let leftDiagonal = [];
    for (let i = 0; i < numRows; i++) {
        rightDiagonal.push(matrix[i][i]);
        leftDiagonal.push(matrix[i][numCols-(i+1)]);
    }
    return [rightDiagonal, leftDiagonal]
}

/**
 * Checks if all elements of a row is equal.
 * @param {array} row 
 * @returns boolean
 */
const isAllEquals = (row) => row.every(val => val === row[0]);

/**
 * Checks who has won the TicTacToe game, or if it ended in a draw.
 * @param {array} matrix - 2D NxN matrix
 * @returns winner ("X" or "O") or "Draw"
 */
const TicTacToeWin = (matrix) => {
    const transposeMatrix = transpose(matrix);
    const diagonals = getDiagonals(matrix);
    const allElementsMatrix = matrix.concat(transposeMatrix).concat(diagonals);
    for (row of allElementsMatrix) {
        if (isAllEquals(row)) {
            if (row[0] !== "E") {
                return row[0];
            }
        }
    }
    return "Draw";

}

array1 = [
    ["X", "O", "X"],
    ["O", "X",  "O"],
    ["O", "X",  "X"]
];

array2 = [
    ["O", "O", "O"],
    ["O", "X", "X"],
    ["E", "X", "X"]
];

array3 = [
    ["X", "X", "O"],
    ["O", "O", "X"],  
    ["X", "X", "O"]
];


console.log(transpose(array1));
console.log(getDiagonals(array1));
console.log(TicTacToeWin(array1));
console.log(TicTacToeWin(array2));
console.log(TicTacToeWin(array3));