
/**
 * Counts the number of digits in the given number (without use of strings).
 * Works for both floats and integers.
 * @param {number} number 
 * @returns count of digits
 */
const num_of_digits = (number) => {
    if (typeof(number) !== "number" || number === null) {
        throw new Error("Your input must be of type number!")
    }
    // there will always be at least one digit present in the number
    let numDigits = 1
    // if float, multiply with ten until whole
    while (number%1 !== 0) {
        number *= 10
    }
    // count the number of digits
    while (number/10 >= 1) {
        numDigits++;
        number /= 10;
    }
    return numDigits
}

console.log(`The number of digits in 1000 is : ${num_of_digits(1000)}`);
console.log(`The number of digits in 12 is : ${num_of_digits(12)}`);
console.log(`The number of digits in 1305981031 is : ${num_of_digits(1305981031)}`);
console.log(`The number of digits in 0 is : ${num_of_digits(0)}`);
console.log(`The number of digits in 1269 is : ${num_of_digits(1269)}`);
console.log(`The number of digits in 489 is : ${num_of_digits(489)}`);
console.log(`The number of digits in 567598103 is : ${num_of_digits(567598103)}`);
console.log(`The number of digits in 52 is : ${num_of_digits(52)}`);
console.log(`The number of digits in 52.67 is : ${num_of_digits(52.67)}`);
