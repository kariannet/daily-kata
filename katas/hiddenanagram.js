/**
 * Checks if a character is a letter.
 * @param {string} inputLetter - a single character
 * @returns boolean - true if the input character is a letter
 */
 const isLetter = (inputLetter) => {
    if (inputLetter.match(/[A-Za-z]/)) return true; // O(?)
    else return false;
}

const removePunctuationDigitsAndSpace = (str) => {
    return str.toLowerCase().replace(/[^a-z]/g, "")
}

const doesMatchLetters = (string1, string2) => {
    string1 = string1.split("").sort().toString();
    string2 = string2.split("").sort().toString();

    return (string1 === string2)
}

const hiddenAnagram = (firstString, secondString) => {
    firstString = removePunctuationDigitsAndSpace(firstString);
    secondString = removePunctuationDigitsAndSpace(secondString);

    let stop = firstString.length - secondString.length
    if (stop < 1) {
        stop = 1;
    }

    for (let i = 0; i < stop; i++) {
        const tempString = firstString.slice(i, i+secondString.length)
        if (doesMatchLetters(tempString, secondString)) {
            return tempString
        }
    }
    return "Not a match"
}

console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"))
console.log(hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison"))
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"))
console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"))
console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"))
console.log(hiddenAnagram("Slopp!ed", "Lop-ped"))
