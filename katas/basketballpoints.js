// function that computes the final points for one team, given their 2- and 3-pointers scored 
// during a basketball game
function pointCount(numTwoPointers, numThreePointers) {
    const total = numTwoPointers*2 + numThreePointers*3
    console.log(total)
    document.writeln("<p>" + total + "</p>")
}

pointCount(1, 1)
pointCount(7, 5)
pointCount(38, 8)
pointCount(0, 1)
pointCount(0, 0)
