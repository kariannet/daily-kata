
const getLength = (arrayElement) => {
    let count = 0;
    for (elem of arrayElement) {
        if (elem instanceof Array) {
            count += getLength(elem);
        }
        else {
            count++;
        }
    }
    return count;
}

/// const getLength = arrayElement => arrayElement.flat(Infinity).length;

thisArray = [1, 2, 3, [4, 5, 6, [7, 8, 9]], "10"];
anotherArray = ["a", 0, [3, 5, 10], [4, [5, 6], "6"], "l", [11]];
emptyArray = [];
document.writeln(getLength(thisArray));
document.writeln(getLength(anotherArray));
document.writeln(getLength(emptyArray));