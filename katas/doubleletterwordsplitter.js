// This function recieves a word (type String) as an argument, and splits the word
// where any double letter is found and return an array of the split word. If no 
// repeated letters are found, it simply returns an empty array
function splitOnDoubleLetter(word) {
    const result = []; // resulting array
    let lowerWord = word.toLowerCase(); // value to store the remains of the word if split
    let newStart = 0; // value to update "start" index
    for (let i = 1; i < lowerWord.length; i++) {
        if (lowerWord[i] === lowerWord[i-1]) {
            result.push(word.slice(newStart, i));
            newStart = i;
        }
    }
    if (result.length > 0) { // add the rest of the word to array if it has been split
        result.push(word.slice(newStart));
    }
    return result;
}

console.log(splitOnDoubleLetter('Letter'));
console.log(splitOnDoubleLetter('Really'));
console.log(splitOnDoubleLetter('Happy'));
console.log(splitOnDoubleLetter('Shall'));
console.log(splitOnDoubleLetter('Tool'));
console.log(splitOnDoubleLetter('Mississippi'));
console.log(splitOnDoubleLetter('Easy'));

