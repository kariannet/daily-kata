// Prints each number from 1 to the given "stop"-number n on a new line.
// For each multiple of 3, it prints "Fizz" instead of the number.
// For each multiple of 5, it prints "Buzz" instead of the number.
// For numbers which are multiples of both 3 and 5, it prints "FizzBuzz"
// instead of the number.
function classicFizzBuzz(n) {
    for (let i = 1; i <= n; i++) {
        const result = checkFizzBuzz(i);
        document.writeln("<p>" + result + "</p>");
        console.log(result);
    }
}

function checkFizzBuzz(number) {
    if (number%3 > 0 && number%5 > 0) { // most common case at top
        return(number.toString());
    }
    let result = "";
    if (number%3 === 0) {
        result += "Fizz";
    }
    if (number%5 === 0) {
        result += "Buzz";
    }
    return result;
}

classicFizzBuzz(20);