/**
 * Counts the number of boomerangs in the input array. A boomerang has the form
 * [x, y, x], where x and y is any number but not equal.
 * @param {Array} inputArray 
 * @returns Integer - count of number of boomerangs in array.
 */
const countBoomerangs = (inputArray) => {
    let count = 0;
    for (let i=0; i < inputArray.length-2; i++) {
        
        if (inputArray[i] === inputArray[i+2] && inputArray[i] !== inputArray[i+1]) {
            count++;
        }
    }
    return count;
}

console.log(countBoomerangs([9, 5, 9, 5, 1, 1, 1]));
console.log(countBoomerangs([5, 6, 6, 7, 6, 3, 9]));
console.log(countBoomerangs([4, 4, 4, 9, 9, 9, 9]));
console.log(countBoomerangs([1, 7, 1, 7, 1, 7, 1]));
