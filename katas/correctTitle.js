/**
 * Takes a sentence and returns it in "title"-style:
 * 1. words and, the, of and in are in lowercase.
 * 2. all other words are capitalized
 * 3. there is a single space behind commas
 * 4. the title ends with a period.
 * 5. hyphenated words are treated as separate words.
 * 
 * @param {*} title 
 * @returns a string.
 */
const correctTitle = (title) => {
    let resultString = "";
    title = title.toLowerCase;
    title = title.replace(",", ", ");
    const stringArray = title.split(" ");
    for (let word of stringArray) {
        const hyphenArray = word.split("-");
        if (hyphenArray.length > 1) {
            resultString = resultString + wordChange(hyphenArray[0]) + "-" + wordChange(hyphenArray[1]);
        }
        else {
            resultString += wordChange(word);
        }
        resultString += " ";
    }
    resultString = resultString.slice(0, resultString.length -1);

    if (resultString[resultString.length-1] != ".") {
        resultString += ".";
    }
    return resultString;
};

/**
 * String manipulation. Words "and", "the", "of" and "in" are kept lowercase.
 * Other words are capitalized.
 * @param {*} word 
 * @returns a string.
 */
const wordChange = (word) => {
    const lowercaseWords = ["and", "the", "of", "in"];
    if (lowercaseWords.includes(word)) {
        return word;
    }
    return word.charAt(0).toUpperCase() + word.slice(1);
}


document.writeln(`<p> ${correctTitle("jOn SnoW, kINg IN thE noRth")} </p>`);
document.writeln(`<p> ${correctTitle("sansa stark,lady of winterfell.")} </p>`);
document.writeln(`<p> ${correctTitle("TYRION LANNISTER, HAND OF THE QUEEN.")} </p>`);
document.writeln(`<p> ${correctTitle("FAKe CharaCter-eLSA, QUEEN of THE FROst.")} </p>`);