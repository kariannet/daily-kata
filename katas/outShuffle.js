/**
 * Out-shuffles an array. Also called Faro shuffle or a perfect shuffle.
 * It is a controlled shuffle-method where
 * @param {Array} inputArray 
 * @returns 
 */
const outShuffle = (inputArray) => {
    const half = parseInt(inputArray.length/2);
    const firstHalf = inputArray.slice(0, half).reverse();
    const lastHalf = inputArray.slice(half).reverse();
    const result = [];
    for (let i = 0; i < inputArray.length; i++) {
        if (i%2 == 0) {
            result.push(firstHalf.pop());
        }
        else {
            result.push(lastHalf.pop());
        }
    }
    return result;
}

/**
 * Creates an array of elements 1 to number, then counts the number of out-shuffles
 * necessary to return the array to its original order.
 * @param {int} number - must be even
 * @returns an integer
 */
const outShuffleCount = (number) => {
    if (number < 2 || number%2 !== 0) {
        throw "Invalid number. Must be even and greater than or equal to 2."
    }
    const cardDeck = createArray(number);
    console.log(cardDeck);
    let count = 0;
    let shuffleArray = cardDeck;
    console.log(shuffleArray);
    do {
        shuffleArray = outShuffle(shuffleArray);
        count ++;
    }
    while (!arrayIsEqual(cardDeck, shuffleArray));
    return count;
};

/**
 * Creates array with numbers 1 to number.
 * @param {int} stopNumber 
 * @returns Array
 */
const createArray = (stopNumber) => {
    let result = [];
    for (let i=1; i < stopNumber+1; i++) {
        result.push(i);
    }
    return result;
};

/**
 * Checks if two arrays are equal.
 * @param {Array} arr1 
 * @param {Array} arr2 
 * @returns boolean
 */
const arrayIsEqual = (arr1, arr2) => {
    if (arr1 === arr2) return true;
    if (arr1 == null || arr2 == null) return false;
    if (arr1.length !== arr2.length) return false;

    for (let i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) return false;
    }
    return true;
}

const arr1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const arr2 = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k"];

document.writeln(`<p> ${outShuffle(arr1)} </p>`);
document.writeln(`<p> ${outShuffle(arr2)} </p>`);

let arr3 = outShuffle(arr1);
document.writeln(`<p> ${arr3} </p>`);
arr3 = outShuffle(arr3);
document.writeln(`<p> ${arr3} </p>`);
arr3 = outShuffle(arr3);
document.writeln(`<p> ${arr3} </p>`);
arr3 = outShuffle(arr3);
document.writeln(`<p> ${arr3} </p>`);
arr3 = outShuffle(arr3);
document.writeln(`<p> ${arr3} </p>`);
arr3 = outShuffle(arr3);
document.writeln(`<p> ${arr3} </p>`);

document.writeln(`<p> ${outShuffleCount(10)} </p>`);
document.writeln(`<p> ${outShuffleCount(20)} </p>`);