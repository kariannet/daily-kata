/**
 * Checks if a character is a letter.
 * @param {string} inputLetter - a single character
 * @returns boolean - true if the input character is a letter
 */
const isLetter = (inputLetter) => {
    if (inputLetter.match(/[A-Za-z]/)) return true; // O(?)
    else return false;
}

/**
 * Caesar's Cipher, encrypts messages by shifting each letter by 
 * the given number of letters. If the shift takes you past the end 
 * of the alphabet, it just rotates back to the front of the alphabet.
 * @param {string} s - string to encrypt
 * @param {number} k - number to shift
 * @returns string - encrypted message
 */
const caesarCipher = (s, k) => {
    const alphabet = "abcdefghijklmnopqrstuvwxyz";

    let resultString = "";

    for (character of s) { // O(n)
        if (isLetter(character)) { // O(?)

            unicode = character.charCodeAt(0) + k;

            // const index = alphabet.indexOf(character.toLowerCase()); //O(26) = O(1)
            // let newIndex = index + k;

            if (unicode > 122) {
                unicode = (unicode - 122) + 96;
            }

            resultString += String.fromCharCode(unicode);
            // if (newIndex > (alphabet.length-1)) {
            //     newIndex = newIndex - alphabet.length;
            // }
            // if (character === character.toUpperCase()) {
            //     resultString += alphabet[newIndex].toUpperCase();
            // }
            // else {
            //     resultString += alphabet[newIndex]
            // }
        }
        else {
            resultString += character;
        }
    }
    return resultString;
}

console.log(caesarCipher("middle-Outz", 2));
console.log(caesarCipher("Always-Look-on-the-Bright-Side-of-Life", 5));
console.log(caesarCipher("A friend in need is a friend indeed", 20));